# Simple Calendar

This repository includes a fairly simple calendar using HTML, CSS and
JavaScript together.

## License

This project is release under the terms of the MIT Open Source license. See
the included *LICENSE.txt* for more details. 
